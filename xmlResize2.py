import xml.etree.ElementTree as ET
import os
import sys


FACTOR = 0.4

dir_path = os.getcwd()
print('init xml resize')

for filename in os.listdir(dir_path + '/resize_images'):
    # If the images are not .jpg images, change the line below to match the image type.
    if filename.endswith(".xml"):
        # abrir arquivo xml
        print('resize_images/' + filename)
        tree = ET.parse('resize_images/' + filename)
        root = tree.getroot()
        new_width = int(float(root.find('size/width').text) * FACTOR)
        new_height = int(float(root.find('size/height').text) * FACTOR)
        root.find('size/width').text = str(new_width)
        root.find('size/height').text = str(new_height)

        for bndbox in root.iter('bndbox'):
            for dim in bndbox:
                new_dim = int(float(dim.text) * FACTOR)
                print('new dim ', new_dim)
                dim.text = str(new_dim)
                print('new dim text', dim.text)

        # print('finished xml resize')
        tree.write('resize_images/' + filename)
