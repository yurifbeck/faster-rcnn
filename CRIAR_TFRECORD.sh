#!/bin/bash

echo "Criar TFRECORD para dados"
echo
echo "O script está executando do diretório:"
pwd

echo
echo "Este script foi testado com Python 3.6"
echo "Versao do python instalada"
alias python3='/usr/bin/python3'
python3 --version
alias pip3='/usr/bin/pip3'

echo "Caso estiver diferente (Python 3.7 ou 2), verificar instalacao"

echo

# # CRIAR ENVIRONMENT
# echo "Criando enviroment conda com pacotes necessarios:"
# echo "..."

# conda create --name data_preprocessing python=3.6
# source activate data_preprocessing
# __________ NAO FUNCIONA PQ O PRECISA ABRIR OUTRO BASH PRA DAR CERTO

read -p "As imagens estao na pasta 'colocar_imagens_e_xml_aqui? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "Arquivos dentro da pasta"
ls colocar_imagens_e_xml_aqui
DIRETORIO='colocar_imagens_e_xml_aqui/'
echo $DIRETORIO
echo
else
echo
read -p "Colocar imagens e arquivos dentro da pasta antes de prosseguir"
exit 0
fi

# INSTALAR BIBLIOTECAS
read -p "Deseja instalar todas bibliotecas necessarias (precisa apenas na primeira vez)? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "Baixando pacotes necessarios"
echo
# BAIXAR PACOTES NECESSARIOS
apt-get install -y --no-install-recommends --fix-missing\
    ca-certificates \
    build-essential \
    git \
    wget \
    unzip \
    python-setuptools \
    python-pil \
    python-lxml \
    python-tk \
    protobuf-compiler \
    libgtk2.0-dev

pip3 install jupyter \
    numpy \
    matplotlib \
    Cython \
    pandas \
    opencv-python \
    contextlib2 \
    six \
    Pillow \
    scipy \
    scikit-image \
    Shapely \
    imgaug

# INSTALAR COCO API
pip3 install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI

# INSTALAR PROTOBUFF (precisa estar na pasta faster-rcnn/models/research)
cd models/research
wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip
unzip protobuf.zip
./bin/protoc object_detection/protos/*.proto --python_out=.
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
cd ../..
fi


# IMAGE AUGMENTATION
read -p "Deseja aplicar image augmentation? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "Baixando pacotes necessarios"
echo
# BAIXAR PACOTES NECESSARIOS
pip3 install imgaug
echo
echo "Aplicando image augmentation"
python3 imgAugmention2.py
DIRETORIO='augmented_labeled_images/'
echo

# IMAGE SPATION RESOLUTION - CASO TENHA AUGMENTATION
read -p "Deseja diminuir a resolucao das imagens? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "Reduzindo resolucao"
echo
cp augmented_labeled_images/* resize_images
python3 imgResize2.py
python3 xmlResize2.py
DIRETORIO='resize_images/'
fi

else
# IMAGE SPATION RESOLUTION - CASO ###NAO#### TENHA AUGMENTATION
read -p "Deseja diminuir a resolucao das imagens? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "Reduzindo resolucao"
echo
cp colocar_imagens_e_xml_aqui/* resize_images
python3 imgResize.py
echo
python3 xmlResize.py
echo
DIRETORIO='resize_images/'
fi

fi


# CRIAR TEST E TRAIN
echo
read -p "Deseja misturar dados e criar subset de train e test? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
echo "diretorio com imagens e XMLs = $DIRETORIO"
python3 split_data2.py $DIRETORIO
echo
echo "Dados misturados dentro da pastas Images/test e Images/Train"
echo
# entrar em cada pasta e forcar que .JPG(maisculo) seja .jpg (minusculo)
cd images/test
find . -name '*.*' -exec sh -c '
  a=$(echo "$0" | sed -r "s/([^.]*)\$/\L\1/");
  [ "$a" != "$0" ] && mv "$0" "$a" ' {} \;
cd ../train
find . -name '*.*' -exec sh -c '
  a=$(echo "$0" | sed -r "s/([^.]*)\$/\L\1/");
  [ "$a" != "$0" ] && mv "$0" "$a" ' {} \;
cd ../..
fi

# CRIAR CSV A PARTIR DO XML
echo
read -p "Deseja CRIAR CSV A PARTIR DO XML? [y/n]"
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo
python3 xml_to_csv.py
echo
echo "csv criados dentro da pastas Images"
echo
fi


# CRIAR LABELMAP
echo
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
read -p "Digite qual o nome do objeto (escrever exatamente como foi dado durante a anotacao, com maiusculas e minusculas)"
label=$REPLY
echo
echo "Sera criado arquivos TFRecord para detectar o objeto chamado:"
echo "$label"

# CRIAR TF RECORD
echo
mkdir -p dados
python3 generate_tfrecord2.py --object=$label --csv_input=images/train_labels.csv --image_dir=images/train --output_path=dados/train.record
python3 generate_tfrecord2.py --object=$label --csv_input=images/test_labels.csv --image_dir=images/test --output_path=dados/test.record
echo
echo "TFRecords criados dentro da pasta DADOS"
echo
echo "Criando labelmap.pbtx"
echo "item {
  id: 1
  name: '$label'
}" > dados/labelmap.pbtx


