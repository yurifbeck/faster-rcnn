# SCRIPT VAI DAR ERRO EM WINDOWS DEVIDO O PATH USAR '\' inves de '/'S

import imgaug as ia
from imgaug import augmenters as iaa
import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import glob
from PIL import Image
from scipy import ndimage
import copy
import xml.etree.ElementTree as ET


# FUNCOES
def verify_image(img_file):
     try:
        v_image = Image.open(img_file)
        v_image.verify()
        return True
        #is valid
        #print("valid file: "+img_file)
     except OSError:
        return False

def aplicar_aug(img, seq_det, n=10):
    orig_images = []
    # cria n copias da mesma imagem
    for i in range(0, n):
        new_img = img.copy()
        orig_images.append(new_img)
    # aplica augmentation
    img_aug = seq_det.augment_images(orig_images)
    return img_aug
    
def salvar_imagens(imagens, filename, dest_folder='augmented_labeled_images'):
    if not (os.path.exists(dest_folder)):
        os.mkdir(dest_folder)
    for i, img in enumerate(imagens):
        # print(filename, i)
        cv2.imwrite('{}/{}_{}.jpg'.format(dest_folder, filename,i), img)

def pegar_bboxes(xml_path):
    bboxes = []
    # print('abrindo file', xml_path)
    tree = ET.parse(xml_path)
    root = tree.getroot()

    for bndbox in root.iter('object'):
        xmin = int(bndbox.find('bndbox/xmin').text)
        ymin = int(bndbox.find('bndbox/ymin').text)
        xmax = int(bndbox.find('bndbox/xmax').text)
        ymax = int(bndbox.find('bndbox/ymax').text)
        xml_label = bndbox.find('name').text
#         box = [xmin, ymin, xmax, ymax]
        box = ia.BoundingBox(x1=xmin, y1=ymin, x2=xmax, y2=ymax, label=xml_label)
        # print('box orignal', box)
        bboxes.append(box)

    # print('pegar bboxes', bboxes)

    return bboxes

def aplicar_bbox_aug(bbox, seq_det, n=10):
    # print('\n aplicar bbox, recebendo bbox = ', bbox)
    orig_bbox = []
    # cria n copias da mesma bbox
    for i in range(n):
        new_bbox = copy.deepcopy(bbox)
        orig_bbox.append(new_bbox)
    # aplica augmentation
    #print('\n orig_bbox len', len(orig_bbox), 'cada um com ', len(orig_bbox[0].bounding_boxes))
    bbox_aug = seq_det.augment_bounding_boxes(orig_bbox)
    # print(bbox_aug[0])
    #print('\n aplicar aug_bbox', bbox_aug[0].bounding_boxes)

    #for bbox_in_image in bbox_aug:
        #print('\n', bbox_in_image)
        #bbox_in_image.remove_out_of_image().cut_out_of_image()
        #print('\n', bbox_in_image)
    return bbox_aug

def criar_xml(todas_aug_bboxes, filename, dest_folder='augmented_labeled_images', n=10):
    if not (os.path.exists(dest_folder)):
        os.mkdir(dest_folder)
        # pegar apenas o nome do arquivo, sem extensao e path
    head, tail = os.path.split(filename)
    name, ext = os.path.splitext(tail)
    w_shape = (aug_bboxes[0].shape)[1]
    h_shape = (aug_bboxes[0].shape)[0]
    print('Numero de arquivos a serem criados:', len(todas_aug_bboxes))
    for i, bb_cada_imagem in enumerate(todas_aug_bboxes):
        filtered = bb_cada_imagem.cut_out_of_image().remove_out_of_image()
        new_filename = '{}_{}'.format(name, i)
        root = ET.Element("annotation")
        # filename
        filename_element = ET.Element('filename')
        filename_element.text = new_filename + '.jpg'
        root.append(filename_element)
        # size
        size_element = ET.SubElement(root, 'size')
        width_element = ET.SubElement(size_element, 'width')
        width_element.text = str(w_shape)
        height_element = ET.SubElement(size_element, 'height')
        height_element.text = str(h_shape)

        # object
        for bb in filtered.bounding_boxes:
            object_element = ET.SubElement(root, 'object')
            name_element = ET.SubElement(object_element, 'name')
            name_element.text = bb.label
            bndbox_element = ET.SubElement(object_element, 'bndbox')
            xmin_element = ET.SubElement(bndbox_element, 'xmin')
            xmax_element = ET.SubElement(bndbox_element, 'xmax')
            ymin_element = ET.SubElement(bndbox_element, 'ymin')
            ymax_element = ET.SubElement(bndbox_element, 'ymax')
            xmin_element.text = str(bb.x1_int)
            xmax_element.text = str(bb.x2_int)
            ymin_element.text = str(bb.y1_int)
            ymax_element.text = str(bb.y2_int)

        tree = ET.ElementTree(root)
        tree.write('{}/{}.xml'.format(dest_folder, new_filename))



def salvar_xml(aug_bboxes, filename, dest_folder='augmented_labeled_images', n=10):
    if not (os.path.exists(dest_folder)):
        os.mkdir(dest_folder)
    # abrir xml original, deste iremos criar varios outros iguais mudando as bbox
    tree = ET.parse(filename)
    # pegar apenas o nome do arquivo, sem extensao e path
    head, tail = os.path.split(filename)
    name, ext = os.path.splitext(tail)
    # manipulacoes xml
    root = tree.getroot()
    # pegar qnts bbxes tem em cada xml
    n_obj = len(root.findall('object'))
    # iterar cada bbox do xml
    print(filename)
    #print(aug_bboxes.bounding_boxes[0])
    

    counter = 0
    for i,xml_aug in enumerate(range(n)):
        new_filename = '{}_{}'.format(name, i)
        root[1].text = str('{}.jpg'.format(new_filename))
        #print('bounding_boxessssss', aug_bboxes[i].bounding_boxes)
        j = 0
        aug_bboxes = [aug_bboxes[0].remove_out_of_image().cut_out_of_image()]
        print(aug_bboxes[0])
        print('qtn bbox aug', len(aug_bboxes[0].bounding_boxes))
        print('qnt bbox no xml', (root.iter('bndbox')))
        for bndbox in root.iter('bndbox'):
             # pegar o aug_bbox
            print(j, aug_bboxes[0].bounding_boxes[j])
            aug_bbox = aug_bboxes[0].bounding_boxes[j]
            # print(aug_bbox)
            # pegar valores do aug_bbox
            xmin, ymin , xmax, ymax = aug_bbox.x1_int, aug_bbox.y1_int, aug_bbox.x2_int, aug_bbox.y2_int
            # modificar o xml para os valores do aug_bbox
            bndbox[0].text = str(xmin)
            bndbox[1].text = str(ymin)
            bndbox[2].text = str(xmax)
            bndbox[3].text = str(ymax)
            #print('counter', counter, 'aug_bbox = ', aug_bbox)
            j += 1

        tree.write('{}/{}.xml'.format(dest_folder, new_filename))


ia.seed(1)
seq = iaa.Sequential([
    iaa.Fliplr(0.5), # horizontal flips
    iaa.Crop(percent=(0, 0.1)), # random crops
    # Small gaussian blur with random sigma between 0 and 0.5.
    # But we only blur about 50% of all images.
    iaa.Sometimes(0.5,
        iaa.GaussianBlur(sigma=(0, 0.5))
    ),
    # Strengthen or weaken the contrast in each image.
    iaa.ContrastNormalization((0.75, 1.5)),
    # Add gaussian noise.
    # For 50% of all images, we sample the noise once per pixel.
    # For the other 50% of all images, we sample the noise per pixel AND
    # channel. This can change the color (not only brightness) of the
    # pixels.
    iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
    # Make some images brighter and some darker.
    # In 20% of all cases, we sample the multiplier once per channel,
    # which can end up changing the color of the images.
    iaa.Multiply((0.8, 1.2), per_channel=0.2),
    # Apply affine transformations to each image.
    # Scale/zoom them, translate/move them, rotate them and shear them.
    # iaa.Affine(
    #     scale={"x": (0.8, 0.2), "y": (0.8, 0.2)},
    #     translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
    #     rotate=(-15, 15),
    #     shear=(-8, 8)
    # )
], random_order=True) # apply augmenters in random order


seq_det = seq.to_deterministic()

# inicializar lista de imagens
img_list = []
xml_path_list = []
bbox_list = []

# Carregar imagens da pasta
if(os.path.exists('./raw_images')):
    print('Pasta imagens encontrada')
    fps = glob.glob("raw_images/*.jpg")
    fps.extend(glob.glob("raw_images/*.JPG"))
    #fps = glob.glob("raw_images/*.JPG")
    print('Quantidades de imagens:', len(fps))

    # para cada jpg presente na pasta
    for img in fps:
        # verificar se imagem esta corrompida, caso nao colocar na lista de imagens
        if verify_image(img):
            # ler a imagem
            img_file = cv2.imread(img)
            img_list.append(img_file)
            # pegar o nome do arquivo
            head, tail = os.path.split(img)
            filename, extension = os.path.splitext(tail)
            # aplicar augmentation
            aug_imgs = aplicar_aug(img_file, seq_det)
            # salvar arquivos produzidos 
            salvar_imagens(aug_imgs, filename)
            # verificar se a imagem tem bounding box
            xml_filename = os.path.join(head, filename + '.xml')
            if os.path.isfile(xml_filename):
                xml_path_list.append(xml_filename)
                # caso sim, extrair coordenadas do xml
                bboxes = pegar_bboxes(xml_filename)
                bbs = ia.BoundingBoxesOnImage(bboxes, shape=img_file.shape)
                # aplicar augmentaion
                aug_bboxes = aplicar_bbox_aug(bbs, seq_det)
                # salvar aug em xmls
                criar_xml(aug_bboxes, xml_filename)
                # image_before = bbs.draw_on_image(img_file, thickness=2)
                # image_after = aug_bboxes[0].draw_on_image(aug_imgs[0], thickness=2, color=[0, 0, 255])
                # fig, ax = plt.subplots(1,3,figsize=(18, 16))
                # ax[0].imshow(cv2.cvtColor(aug_imgs[0], cv2.COLOR_BGR2RGB))
                # ax[1].imshow(cv2.cvtColor(image_before, cv2.COLOR_BGR2RGB))
                # ax[2].imshow(cv2.cvtColor(image_after, cv2.COLOR_BGR2RGB))
                # plt.show()
                
        else:
            print('Imagem falhou:', img)

print('Quantidade de imagens para augmentation:', len(img_list))
print('Quantidade de bboxes para augmentation:', len(xml_path_list))

