import numpy as np
import cv2
import os
import sys


FACTOR = 0.4
    
dir_path = os.getcwd()

for filename in os.listdir(dir_path  + '/resize_images'):
    # If the images are not .jpg images, change the line below to match the image type.
    if filename.endswith(tuple([".jpg", ".JPG"])):
        print(filename)
        image = cv2.imread(filename)
        resized = cv2.resize(image, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_AREA)
        cv2.imwrite(filename,resized)

