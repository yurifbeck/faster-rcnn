# Faster RCNN

Projeto com utilidades para implementação do algorítmo FASTER RCNN com:
- DOCKER (Ubuntu 16.04)
- UBUNTU 16.04
- TENSORFLOW OBJ DETECTION MODEL
- DATA AUGMENTATION com IMGAUG
- LABELING DE IMG  usando LabelImg

## TUTORIAL DE COMO UTILIZAR

Recomendo dar uma lida por completo antes de começar.

### Downloads

1. Baixe o projeto [TensorFlow Models](https://github.com/tensorflow/models) e coloque a pasta *models* na pasta principal deste projeto.

### Docker (OPCIONAL)

Docker serve para que não haja problemas do tipo *uai, na minha máquina funciona*. O Docker vai gerar um container com as exatas configurações para que este pacote rode sem problemas de biblioteca faltantes, certas configurações, versão de python errada e outros...

1. Instalar docker

```shell
sudo apt-get install docker-ce docker-compose
```
2. Construir imagem a partir do arquivo Dockerfile. Dentro da pasta principal:

```shell
docker-compose up
```

Com isso teremos uma imagem chamada *object_detection* com tudo configurado. Iremos usar essa imagem para rodar um container nos próximos passos.

### Pre-processamento de dados

1. Coloque *todas* suas imagens não tradadas na pasta *raw_images*. Mias tarde iremos separar os dados para treinamento e teste.

2. **Data Labeling** - Baixe o [LabelImg](https://github.com/tzutalin/labelImg). Abra o programa e selecione o diretório *raw_images*. Comece a desenhar bounding boxes em todas as imagens. Esse é o processo mais demorado. Existem atalhos de teclado neste software para agilizar o processo. Para cada imagem será gerado um arquivo _xml_ contendo as coordenadas de cada box. Ao final desta etapa, deve-se ter algo semelhante a isto:

```shell
ls raw_images/
DJI_0001.jpg  DJI_0002.xml  DJI_0006.jpg  DJI_0009.xml
DJI_0001.xml  DJI_0004.jpg  DJI_0006.xml  DJI_0013.jpg
DJI_0002.jpg  DJI_0004.xml  DJI_0009.jpg  DJI_0013.xml
```

4. **Data augmentation** (OPCIONAL)- Com todas imagens com label, iremos usar o script `imgAugmentation.py`. Por padrão, o script aplica **aleatóriamente** os efeitos abaixo em uma única imagem, transformando ela em **10**: 

- Deslocamento (translate)
- Esticamento (shear)
- Rotação (rotate)
- Escala (scale)
- Espelhamento horizontal (flip)
- Embaçamento (gaussian blur)
- Ruído (noise)
- Adição de canal (multiply)

Caso queira mudar algo ou adicionar mais, modificar o arquivo `imgAugmentation.py`.

Inicie o docker container *object_detection* com o comando:

```shell
docker-compose run object_detetion /bin/bash
```

- `-it` serve para fornecer um terminal com o container
- `-v local_meu_pc:local_container` serve para compartilhar arquivos entre seu computador e o container. Neste caso `${PWD}` é um comando para pegar o diretório local, e ele aparecerá dentro do container no diretório `home/faster-rcnn`

Assim que entrar no container digite o comando 

```shell
python3 imgAugmentation.py
```

5. **Resolution reduction** (OPCIONAL) - As vezes pode-se diminuir a resolução das imagens. Isso vai agilizar muito em todos processos seguintes. Entenda que vocẽ perderá dados e seu modelo poderá piorar. Este script pode ser usado antes ou depois do LabelImg. Coloque todos seus arquivos jpg e XML na pasta resize_images, e navegue para dentro dela, que os scripts estao la dentro

```shell
# copia as imagens aumentadas e seus respectivos xml para a pasta resize_images
cp augmented_labeled_images/* resize_images
cd resize_images
python3 imgResize.py
python3 xmlResize.py
```

Após estes passos, é bom abrir o LabelImg novamente e apontar para o diretorio `resize_images` e verificar se as bounding boxes ficaram boas

6. **Shuffle train and test data** Neste passo iremos dividir todos nossos dados em 75% treino e 25% teste. Esta porcentagem pode ser mudada dentro do arquivo `split_data.py`.

```shell
python3 split_data.py
```

O script irá automaticamente pegar todos os arquivos dentro da pasta *resize_images*, embaralha-los e criar uma copia deles dentro pasta *images/*.

7. Criar CSV a partir do XML. Para gerar o arquivo necessário (TFRecord) no TensorFlow, a gente precisa das coordenadas dos objetos nas imagens em um arquivo CSV. O LabelImg produz coordenadas em XML, então usamos o seguinte comando para converter:

```shell
python3 xml_to_csv.py
```

Com isso, ele produz 2 arquivos dentro da pasta *images* com todos os 

8. **Generate TFRecord** agora podemos gerar o arquivo que final que sera lido pelo TensorFlow. Deve-se **modificar o arquivo** `generate_tfrecord.py` com as classes que se deseja detectar, colocando o nome de cada classe igual no csv. Modificar a parte:

```python
# TO-DO replace this with label map
def class_text_to_int(row_label):
    if row_label == 'minha_classe_1':
        return 1
    elif row_label == 'minha_classe_2':
        return 2
    elif row_label == 'minha_classe_3':
        return 3
```

Com isso podemos executar os scripts para formar o TFRecords, executar:

```shell
python3 generate_tfrecord.py --csv_input=images/train_labels.csv --image_dir=images/train --output_path=train.record
python3 generate_tfrecord.py --csv_input=images/test_labels.csv --image_dir=images/test --output_path=test.record
```

Ao fim teremos os dois arquivos `train.record` e `test.record`

9. **labelmap.pbtxt**. Modificar este arquivo com as informações de suas classes, conforme o exemplo abaixo: 

```json
item {
  id: 1
  name: 'nine'
}

item {
  id: 2
  name: 'ten'
}

```
### Configurar o Modelo

1. Iremos aplicar o *transfer learning*, ou seja, pegar uma rede já treinada e pronta e vamos modificar apenas a última layer para ela responder aos nossos dados. Para tanto, precisamos selecionar o modelo. Pode-se ver uma lista imensa de modelos dentro da pasta `models/research/object_detection/samples/configs`. Quando escolher um, copie o arquivo pra pasta inicial *faster-rcnn*. Este arquivo determina apenas a configuração de parâmetros do modelo.

2. Agora temos que baixar o modelo treinado.

Vá (neste link)[https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md] e baixe o mesmo modelo e coloque na pasta principal.

3. Voltando no arquivo `.config` do passo 1, neste arquivo precisamos mudar vários parâmetros:

- Número de classes

```json
  faster_rcnn {
    num_classes: 3
    ...
```

- Diretórios do modelo, test e train.record e labelmap.pbtxt

```json
  }
  gradient_clipping_by_norm: 10.0
  fine_tune_checkpoint: "/home/faster-rcnn/faster_rcnn_inception_v2_coco_2018_01_28/model.ckpt/model.ckpt"
  from_detection_checkpoint: true
```
4. Colocar arquivos nos locais corretos

```
+data
  -label_map file
  -train TFRecord file
python3 object_detection/export_inference_graph.py --input_type=image_tensor --pipeline_confi_path=object_detection/models/model/faster_rcnn_inception_v2_coco.config --trained_checkpoint_prefix=object_detection/ --output_directory=object_detection/20181121_2  -eval TFRecord file
+models
  + model
    -pipeline config file
    +train
    +eval
```

5. Treinar
If you're using python3 , add list() to category_index.values() in model_lib.py about line 381 as this list(category_index.values()).

From the tensorflow/models/research/ directory

```shell
PIPELINE_CONFIG_PATH={path to pipeline config file}
MODEL_DIR={path to model directory}
NUM_TRAIN_STEPS=50000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1
python3 object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr
```


6. Exportar frozen graph

```shell
python3 object_detection/export_inference_graph.py --input_type=image_tensor --pipeline_config_path=object_detection/models/model/faster_rcnn_inception_v2_coco.config --trained_checkpoint_prefix=object_detection/models/model/model.ckpt-200000 --output_directory=object_detection/20181121_2
```

# Aprendizados

- É possivel que se seu train.tfrecord tiver mais de 3gb, alguns modelos não funcionam acusando falta de memória (isso ocorreu comigo usando GTX 1070 com 8GB). Eu estava forçando muito no imgaug
- Verificar se ao reduzir a resolução da sua imagem você não perde detalhes importantes

