import os
import glob
import pathlib
import random
import shutil

TRAIN = 0.75
TEST = 0.25
src = 'augmented_labeled_images/'
dest = 'images'

all_images = glob.glob(src + '*.jpg')

# Quantidade de imagens para fazer porcentagem
l = len(all_images)

train_size = int(l * TRAIN)
test_size = int(l * TEST)

#aplicar shuffle
random.shuffle(all_images)

# lista de train e test 
train_images = all_images[:train_size]
test_images = all_images[-test_size:]

# criar pastas de treino e teste
pathlib.Path(dest + '/test').mkdir(parents=True, exist_ok=True)
pathlib.Path(dest + '/train').mkdir(parents=True, exist_ok=True)

def verify_xml(img):
     try:
        xml_src = os.path.splitext(img)[0] + '.xml'
        return True
        #is valid
        #print("valid file: "+img_file)
     except OSError:
        return False

# loop para pegar tanto a imagem quanto o xml
for img in train_images:
    print(img)
    try:
        a = pathlib.Path(os.path.splitext(img)[0] + '.xml').resolve
        xml_src = os.path.splitext(img)[0] + '.xml'
        shutil.copy(img, dest + '/train')
        shutil.copy(xml_src, dest + '/train')
    except FileNotFoundError:
        print('no xml found: ', img)

for img in test_images:
    print(img)
    try:
        xml_src = os.path.splitext(img)[0] + '.xml'
        shutil.copy(img, dest + '/test')
        shutil.copy(xml_src, dest + '/test')
    except FileNotFoundError:
        print('no xml found: ', img)


