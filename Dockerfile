# Docker image for running examples in Tensorflow models.
# base_image depends on whether we are running on GPUs or non-GPUs
ARG UBUNTU_VERSION=16.04
FROM ubuntu:${UBUNTU_VERSION}

ARG USE_PYTHON_3_NOT_2=True
ARG _PY_SUFFIX=${USE_PYTHON_3_NOT_2:+3}
ARG PYTHON=python${_PY_SUFFIX}
ARG PIP=pip${_PY_SUFFIX}

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

RUN apt-get update && apt-get install -y \
    ${PYTHON} \
    ${PYTHON}-pip

RUN ${PIP} install --upgrade \
    pip \
    setuptools

ARG TF_PACKAGE=tensorflow
RUN ${PIP} install ${TF_PACKAGE}

RUN apt-get install -y --no-install-recommends --fix-missing\
    ca-certificates \
    build-essential \
    git \
    wget \
    unzip \
    python-setuptools \
#    libsm6 \
#    libxrender-dev \
    python-pil \
    python-lxml \
    python-tk \
    protobuf-compiler \
    libgtk2.0-dev

# Checkout tensorflow/models at HEAD
# RUN git clone https://github.com/tensorflow/models.git /tensorflow/models

RUN ${PIP} install jupyter \
    numpy \
    matplotlib \
    Cython \
    pandas \
    opencv-python \
    contextlib2 \
    six \
    Pillow \
    scipy \
    scikit-image \
    Shapely \
    imgaug

RUN ${PIP} install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI

ADD . home/faster-rcnn
WORKDIR /home/faster-rcnn/models/research

RUN wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip
RUN unzip protobuf.zip
RUN ./bin/protoc object_detection/protos/*.proto --python_out=.

RUN export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

WORKDIR /home/faster-rcnn/ 
